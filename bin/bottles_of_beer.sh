#!/bin/bash
echo "999 bottles of beer on the wall"
sleep 2
echo "999 bottles of beer"
sleep 2
echo "Take one down, pass it around…"
sleep 2

# for i in {998..1}
for i in $(seq 998 -1 1)
do
        echo "$i bottles of beer on the wall"
        sleep 4
        echo ""
        echo "$i bottles of beer on the wall"
        sleep 2
        echo "$i bottles of beer"
        sleep 2
        echo "Take one down, pass it around…"
        sleep 2
done
